# Preview all emails at http://localhost:3000/rails/mailers/notif_mailer
class NotifMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notif_mailer/article
  def article
    NotifMailer.article
  end

  def subscribe
    NotifMailer.subscribe
  end

end
