require 'test_helper'

class SliderControllerTest < ActionDispatch::IntegrationTest
  test "should get article:references" do
    get slider_article:references_url
    assert_response :success
  end

  test "should get title" do
    get slider_title_url
    assert_response :success
  end

  test "should get image" do
    get slider_image_url
    assert_response :success
  end

  test "should get index" do
    get slider_index_url
    assert_response :success
  end

end
