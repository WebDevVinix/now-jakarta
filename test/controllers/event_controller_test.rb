require 'test_helper'

class EventControllerTest < ActionDispatch::IntegrationTest
  test "should get title" do
    get event_title_url
    assert_response :success
  end

  test "should get meta-title" do
    get event_meta-title_url
    assert_response :success
  end

  test "should get meta-des" do
    get event_meta-des_url
    assert_response :success
  end

  test "should get content:text" do
    get event_content:text_url
    assert_response :success
  end

  test "should get start_date:date" do
    get event_start_date:date_url
    assert_response :success
  end

  test "should get end_date:date" do
    get event_end_date:date_url
    assert_response :success
  end

  test "should get map" do
    get event_map_url
    assert_response :success
  end

  test "should get vanue" do
    get event_vanue_url
    assert_response :success
  end

end
