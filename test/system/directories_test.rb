require "application_system_test_case"

class DirectoriesTest < ApplicationSystemTestCase
  setup do
    @directory = directories(:one)
  end

  test "visiting the index" do
    visit directories_url
    assert_selector "h1", text: "Directories"
  end

  test "creating a Directory" do
    visit directories_url
    click_on "New Directory"

    fill_in "Building Name", with: @directory.building_name
    fill_in "City", with: @directory.city
    fill_in "Email", with: @directory.email
    fill_in "Facebook Link", with: @directory.facebook_link
    fill_in "Image", with: @directory.image
    fill_in "Lat", with: @directory.lat
    fill_in "Link Ig", with: @directory.link_ig
    fill_in "Long", with: @directory.long
    fill_in "Map", with: @directory.map
    fill_in "Phone", with: @directory.phone
    fill_in "Postal Code", with: @directory.postal_code
    fill_in "Region", with: @directory.region
    fill_in "Street", with: @directory.street
    fill_in "User Facebook", with: @directory.user_facebook
    fill_in "User Ig", with: @directory.user_ig
    fill_in "Venue Type", with: @directory.venue_type
    fill_in "Website", with: @directory.website
    click_on "Create Directory"

    assert_text "Directory was successfully created"
    click_on "Back"
  end

  test "updating a Directory" do
    visit directories_url
    click_on "Edit", match: :first

    fill_in "Building Name", with: @directory.building_name
    fill_in "City", with: @directory.city
    fill_in "Email", with: @directory.email
    fill_in "Facebook Link", with: @directory.facebook_link
    fill_in "Image", with: @directory.image
    fill_in "Lat", with: @directory.lat
    fill_in "Link Ig", with: @directory.link_ig
    fill_in "Long", with: @directory.long
    fill_in "Map", with: @directory.map
    fill_in "Phone", with: @directory.phone
    fill_in "Postal Code", with: @directory.postal_code
    fill_in "Region", with: @directory.region
    fill_in "Street", with: @directory.street
    fill_in "User Facebook", with: @directory.user_facebook
    fill_in "User Ig", with: @directory.user_ig
    fill_in "Venue Type", with: @directory.venue_type
    fill_in "Website", with: @directory.website
    click_on "Update Directory"

    assert_text "Directory was successfully updated"
    click_on "Back"
  end

  test "destroying a Directory" do
    visit directories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Directory was successfully destroyed"
  end
end
