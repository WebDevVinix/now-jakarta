namespace :db do
  namespace :populate do
    task send_automation_report_banner: :environment do
      Banner.avaiable_to_create_report.each do |x|
        x.update_attribute(:status, "trash")
        NotifMailer.send_report_banner(x).deliver
      end
    end
  end
end
