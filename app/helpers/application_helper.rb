module ApplicationHelper
  def slider_active(i)
    case i
    when 0 then "active"
    else ""
    end
  end

  def article_status(x)
    if x.editor_pick == true
      "Editor's Pick"
    elsif x.featured == true
      "Editor's Pick"
    elsif Category.magazine_issue.map(&:id).include?(x.category_id)
      "Magazine Issue"
    else

    end
  end

  def slider_article_url(x)
    x.article.category.parent.nil? ? dtl_article_without_parent_path(x.article.category.slug, x.article.slug)  : dtl_article_path(x.article.category.parent.slug, x.article.category.slug, x.article.slug, slider:true)
  end

  def article_url(x)
    dtl_article_path(x.category.parent.slug, x.category.slug, x.slug) rescue 'x'
  end

  def sub_category_url(x)
    x.category.parent.nil? ? dtl_category_path( x.category.slug) : dtl_sub_category_path(x.category.parent.slug,  x.category.slug)
  end

  def home_page_banner(x)
    the_banner = Banner.where(position:x).where(status: 'active').first
    unless the_banner.nil?
      unless the_banner.expired_date.nil?
        if the_banner.expired_date > Date.today
          if the_banner.widget.nil? or the_banner.widget.blank?
            link_to banners_counter_path(id: the_banner.id), target: "_blank" do
              image_tag("http://nowjakarta.co.id#{the_banner.image.url}",  class:"w-100")
            end
          else
            the_banner.widget.html_safe
          end
        end
      end
    end
  end

  def home_page_banner_long(x)
    the_banner = Banner.where(position:x).where(status: 'active').first
    unless the_banner.nil?
      unless the_banner.expired_date.nil?
        if the_banner.expired_date > Date.today
          if the_banner.widget.nil? or the_banner.widget.blank?
            "<section class='mt-5 mb-5'>
            <div class='container'>
            <a href='#{banners_counter_path(id: the_banner.id)}' target='_blank'>
            <img src='#{the_banner.image.url}' alt='#{the_banner.title}' class='w-100'>
            </a>
            </div>
            </section>".html_safe
          else
            the_banner.widget.html_safe
          end
        end
      end
    end
  end

  def home_page_popup(x)
    the_banner = Banner.where(position:x).where(status: 'active').first
    unless the_banner.nil?
      unless the_banner.expired_date.nil?
        if the_banner.expired_date > Date.today
          if the_banner.widget.nil? or the_banner.widget.blank?
            "<div class='modal fade' id='popupbanner' tabindex='-1' role='dialog' aria-labelledby='popupbannerLabel' aria-hidden='true'>
              <div class='modal-dialog modal-dialog-centered modal-lg' role='document'>
                <div class='modal-content' >
                  <div class='modal-header p-0 pr-3 pl-3' style='border:0;'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close' style='color:red;'>
                      <span aria-hidden='true'>&times;</span>
                    </button>
                  </div>
                  <div class='modal-body' >
                    <a href='#{banners_counter_path(id: the_banner.id)}' target='_blank'>
                      <img src='#{the_banner.image.url}' alt='#{the_banner.title}' style='width:100%'>
                    </a>
                  </div>
                  <div class='modal-footer justify-content-center'>
                    <a href='#{banners_counter_path(id: the_banner.id)}'  target='_blank' class='btn btn-dark'>READ NOW!</a>
                  </div>
                </div>
              </div>
            </div>".html_safe
          else
            the_banner.widget.html_safe
          end
        end
      end
    end
  end

  def ctr_banner_long(x)
    the_banner = Banner.where(position:x).where(status: 'active').first
    unless the_banner.nil?
      unless the_banner.expired_date.nil?
        if the_banner.expired_date > Date.today
          if the_banner.widget.nil? or the_banner.widget.blank?
            "
            <a href='#{banners_counter_path(id: the_banner.id)}' target='_blank'>
            <img src='#{the_banner.image.url}' alt='#{the_banner.title}' class='w-100'>
            </a>
            ".html_safe
          else
            the_banner.widget.html_safe
          end
        end
      end
    end
  end

  def human_banner_name(pos)
    if ['hp1','hp2'].include? pos
      "Homepage - Featured"
    elsif pos == "hp3"
      "Homepage - Long Feature 1 (under slider)"
    elsif pos == "hp4"
      "Homepage - Long Feature 2 ( Under Featured Section)"
    elsif pos == "hp5"
      "Homepage - Main Theme Long (under main theme)"
    elsif pos == "hp6"
      "Homepage - Latest Long (under Latest Article Section)"
    elsif pos == "hp7"
      "Homepage - Event Long (Under Event)"
    elsif pos == "hp8l"
      "Homepage - Editor Pick Long (under Editor Picks)"
    elsif ['hp8','hp9', 'hp10', 'hp11'].include? pos
      "Homepage - Dine and Explore Jakarta Section"
    elsif pos == "hp9l"
      "Homepage - Video (Above Video)"
    elsif pos == "hp10l"
      "Homepage - Video (Below Video)"
    else pos
  end
end

def timeago(time)
  content_tag(:span, time.iso8601, title: time.iso8601, class: 'timeago')
end

def set_button_print(type)
  case type
  when "print" then "btn-dark"
  when nil  then "btn-dark"
  else "btn-outline-dark"
  end
end

def set_button_digital(type)
  case type
  when "digital" then "btn-dark"
  else "btn-outline-dark"
  end
end

def set_button_newsletter(type)
  case type
  when "newsletter" then "btn-dark"
  else "btn-outline-dark"
  end
end

end
