json.extract! article, :id, :title, :publish, :featured, :editor_pick, :image, :status, :category_id, :writer_id, :slug, :created_at, :updated_at
json.url article_url(article, format: :json)
