json.data(@articles) do |x|
  json.id x.id
  json.title x.title
  json.url dtl_article_path(x.category.parent.slug, x.category.slug, x.slug)
  json.category x.category.title
  json.status x.status
  json.publish x.publish.strftime("%e %B %Y @%H:%M") rescue "-"
  json.action "<a class='btn btn-xs btn-default m-1' href='#{article_path(x)}'>SHOW</a> <a class='btn btn-xs btn-warning m-1' href='#{edit_article_path(x)}'>EDIT</a> <a data-confirm='Are you sure?' class='btn btn-xs btn-danger m-1' rel='nofollow' data-method='delete' href='#{article_path(x)}'>Delete</a>"
end
