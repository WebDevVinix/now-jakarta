json.extract! subscribe, :id, :title, :fullname, :telephone, :phonenumber, :fax, :email, :address, :plan, :created_at, :updated_at
json.url subscribe_url(subscribe, format: :json)
