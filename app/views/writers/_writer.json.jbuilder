json.extract! writer, :id, :fullname, :desc, :main_contributor, :image, :slug, :created_at, :updated_at
json.url writer_url(writer, format: :json)
