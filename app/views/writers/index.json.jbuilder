json.writers(@writers) do |x|
  json.id x.id
  json.fullname x.fullname
  json.image "http://nowjakarta.co.id#{x.image_url}"
  json.desc x.desc
end
