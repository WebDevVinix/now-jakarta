json.extract! slider, :id, :article_id, :title, :image, :index, :created_at, :updated_at
json.url slider_url(slider, format: :json)
