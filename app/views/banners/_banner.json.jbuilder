json.extract! banner, :id, :title, :image, :start_date, :expired_date, :link, :position, :click, :widget, :status, :category_id, :created_at, :updated_at
json.url banner_url(banner, format: :json)
