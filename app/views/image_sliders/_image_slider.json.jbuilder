json.extract! image_slider, :id, :image, :caption, :gallery_id, :created_at, :updated_at
json.url image_slider_url(image_slider, format: :json)
