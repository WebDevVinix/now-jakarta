json.data(@taggeds) do |x|
  json.id x.id
  json.title x.title.html_safe
  json.category x.category.title
  json.url "http://nowjakarta.co.id" + article_url(x)
  json.publish x.publish.strftime("%e %B %Y @%H:%M") rescue "-"
  json.meta x.meta_des
  json.image x.image.regular
end
