json.extract! event, :id, :title, :meta_title, :meta_des, :content, :start_date, :end_date, :map, :vanue, :created_at, :updated_at
json.url event_url(event, format: :json)
