json.data(@tags) do |x|
  json.id "<div class='text-center'>#{x.id}</div>"
  json.name "<div class='text-center'>#{x.name}</div>"
  json.action "<div class='text-center'><a class='btn btn-xs btn-warning m-2' href='#{edit_tag_path(x)}'>Edit</a></div>"
end
