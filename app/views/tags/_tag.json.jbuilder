json.extract! tag, :id, :name
json.url article_url(tag, format: :json)
