json.extract! category, :id, :title, :desc, :parent_id, :main, :slug, :created_at, :updated_at
json.url category_url(category, format: :json)
