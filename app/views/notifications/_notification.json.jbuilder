json.extract! notification, :id, :title, :description, :url, :created_at, :updated_at
json.url notification_url(notification, format: :json)
