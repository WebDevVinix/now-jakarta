json.extract! directory, :id, :building_name, :street, :region, :city, :postal_code, :phone, :website, :user_ig, :link_ig, :user_facebook, :facebook_link, :email, :map, :lat, :long, :image, :venue_type, :created_at, :updated_at
json.url directory_url(directory, format: :json)
