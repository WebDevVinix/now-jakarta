json.extract! device, :id, :endpoint, :p256dh_key, :auth_key, :created_at, :updated_at
json.url device_url(device, format: :json)
