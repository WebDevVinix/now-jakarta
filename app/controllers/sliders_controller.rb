class SlidersController < ApplicationController
  before_action :set_slider, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  before_action :authenticate_user!
  # GET /sliders
  # GET /sliders.json
  def index
    @slider_homepage = Slider.where(type_slider: "homepage").order(index: :asc)
    @slider_magazine_issue = Slider.where(type_slider: "magazine_issue").order(index: :asc)
  end

  # GET /sliders/1
  # GET /sliders/1.json
  def show

  end

  def to_slider
    @slider = Slider.find_by_id(params[:slider_id])

    redirect_to sliders_path, notice: 'Asu has been aprove'
  end

  # GET /sliders/new
  def new
    @slider = Slider.new
  end

  # GET /sliders/1/edit
  def edit
  end



  # POST /sliders
  # POST /sliders.json
  def create
    @slider = Slider.new(slider_params)
    @slider.index = Slider.all.count + 1
    respond_to do |format|
      if @slider.save
        format.html { redirect_to sliders_path, notice: 'Slider was successfully created.' }
        format.json { render :show, status: :created, location: @slider }
      else
        format.html { render :new }
        format.json { render json: @slider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sliders/1
  # PATCH/PUT /sliders/1.json
  def update
    respond_to do |format|
      if @slider.update(slider_params)
        format.html { redirect_to sliders_path, notice: 'Slider was successfully updated.' }
        format.json { render :show, status: :ok, location: @slider }
      else
        format.html { render :edit }
        format.json { render json: @slider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sliders/1
  # DELETE /sliders/1.json
  def destroy
    @slider.destroy
    respond_to do |format|
      format.html { redirect_to sliders_url, notice: 'Slider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def sort
   params[:order].each do |key,value|
     Slider.find(value[:id]).update_attribute(:index,value[:position])
   end
   render :nothing => true
 end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slider
      @slider = Slider.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slider_params
      params.require(:slider).permit(:title, :article_id, :image, :index, :type_slider)
    end
end
