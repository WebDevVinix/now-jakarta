class ApplicationController < ActionController::Base
  before_action :detect_device_variant
  before_action :prepare_meta_tags, :load_counter
  def after_sign_in_path_for(resource_or_scope)
      dashboard_index_path
  end

  $vapid_private =  "6tKH8lmEGWpYZjjNSBMYAwaK46R2UPd8gSDNTTdJ7rs="
  $vapid_public = "BI8zYHMWsy-ETJe8jtVSAXbnXK5SvFjZ3Sv9E-58SRlFgfj4BFZFn7pkO5LiVAdbcShfKqkfV4ZLWaz9uNXSsGY="

  def prepare_meta_tags(options={})
    site_name   = "NOW! JAKARTA"
    title       = "Life in the Capital"
    description = "NOW! Jakarta is a magazine for international visitors & expatriates in Jakarta, focusing on art, culture, dining, hotels and events in Jakarta."
    image       = options[:image] || request.protocol + request.host_with_port + Category.magazine_issue.order(created_at: :asc).last.cover.url
    current_url = "http://nowjakarta.co.id/"

    # Let's prepare a nice set of defaults
    defaults = {
      reverse: true,
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    %w[jakarta news update event lifestyle art culture community ],
      twitter: {
        site_name: site_name,
        site: '@NOW_Jakarta',
        creator: '@NOW_Jakarta',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end

  protected

  def load_counter
    @crn_confirm = ""
    @crn_cured = ""
    @crn_died = ""
  end

  private

  def detect_device_variant
    request.variant = :phone if browser.device.mobile?
    request.variant = :tablet if browser.device.tablet?
  end
end
