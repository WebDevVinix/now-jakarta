class WritersController < ApplicationController
  before_action :set_writer, only: [:show, :edit, :update, :destroy]
  layout 'admin', except: :show
  before_action :authenticate_user!, except: :show
  # GET /writers
  # GET /writers.json
  def index
    @writers = Writer.all
  end

  # GET /writers/1
  # GET /writers/1.json
  def show
    @first = @writer.articles.publish.first
    @all = @writer.articles.where.not(id: @writer.articles.publish.first.id).publish.page(params[:page]).per(5)
    @other = Writer.where.not(id: @writer.id).limit(4).order("random()")

    prepare_meta_tags(
    title: @writer.fullname,
    description: @writer.desc,
    image: @writer.image,
    twitter: {
      site_name: "NOW JAKARTA | #{@writer.fullname || @writer.fullname}" ,
      site: '@NOW_Jakarta',
      creator: '@NOW_Jakarta',
      card: 'summary',
      description:   @writer.desc || "NOW! JAKARTA WRITER",
      title: @writer.fullname || @writer.fullname,
      image: request.protocol + request.host_with_port + @writer.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @writer.slug,
      site_name:  "NOW JAKARTA | #{@writer.fullname || @writer.fullname}" ,
      title: @writer.fullname || @writer.fullname,
      image: request.protocol + request.host_with_port + @writer.image.url,
      type: 'article',
      description:  @writer.desc || "NOW! JAKARTA WRITER"
      })

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /writers/new
  def new
    @writer = Writer.new
  end

  # GET /writers/1/edit
  def edit
  end

  def import
    Writer.import(params[:file])
    redirect_to writers_path, notice: "Writer imported."
  end


  # POST /writers
  # POST /writers.json
  def create
    @writer = Writer.new(writer_params)

    respond_to do |format|
      if @writer.save
        format.html { redirect_to @writer, notice: 'Writer was successfully created.' }
        format.json { render :show, status: :created, location: @writer }
      else
        format.html { render :new }
        format.json { render json: @writer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /writers/1
  # PATCH/PUT /writers/1.json
  def update
    respond_to do |format|
      if @writer.update(writer_params)
        format.html { redirect_to @writer, notice: 'Writer was successfully updated.' }
        format.json { render :show, status: :ok, location: @writer }
      else
        format.html { render :edit }
        format.json { render json: @writer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /writers/1
  # DELETE /writers/1.json
  def destroy
    @writer.destroy
    respond_to do |format|
      format.html { redirect_to writers_url, notice: 'Writer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_writer
      @writer = Writer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def writer_params
      params.require(:writer).permit(:fullname, :desc, :main_contributor, :image, :slug)
    end
end
