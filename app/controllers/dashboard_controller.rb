class DashboardController < ApplicationController
  layout 'admin'
  before_action :authenticate_user!

  def index
    @total_articles = Article.all.count
    @published_articles = Article.publish.count
    @scheduled_articles = Article.on_scheduled.where.not(:status => 'trash').count
    @total_drafted_articles = Article.drafted.count
    @total_active_banners = Banner.running.count
    @total_subscribe_device = Device.all.count
    if Article.popular.present?
      @most_popular_articles = Article.popular.first.title
    end
    @drafted_articles = Article.drafted
    @articles_on_scheduled = Article.on_scheduled.where.not(status: "draft")
    @expired_banners = Banner.where(expired_date: Date.today..Date.today - 7.day)
    @unread_subscription = Subscribe.where(status:[nil])
  end
end
