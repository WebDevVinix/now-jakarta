class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout "admin"
  # GET /notifications
  # GET /notifications.json
  def index
    @notifications = Notification.all
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
  end

  # GET /notifications/new
  def new
    @notification = Notification.new
  end

  # GET /notifications/1/edit
  def edit
  end

  # POST /notifications
  # POST /notifications.json
  def create
    @notification = Notification.new(notification_params)

    respond_to do |format|
      if @notification.save
        format.html { redirect_to notifications_path, notice: 'Notification was successfully created.' }
        Device.all.each do |d|
          begin
            Webpush.payload_send(endpoint: d.endpoint,
              message: "#{@notification.article.title}|#{@notification.article.meta_des}|#{@notification.article.category.parent.slug}/#{@notification.article.category.slug}/#{@notification.article.slug}?source=web-push|https://nowjakarta.co.id#{@notification.article.image}" ,
              p256dh: d.p256dh_key,
              auth: d.auth_key,
              ttl: 12 * 60 * 60,
              vapid: {
                subject: 'mailto:aulya@gmail.com',
                expiration: 12 * 60 * 60,
                public_key: $vapid_public,
                private_key: $vapid_private,
                })
              rescue NoMethodError, RuntimeError, KeyError
                # todo : mark expired endoint
              end
            end
            format.json { render :show, status: :created, location: @notification }
          else
            format.html { render :new }
            format.json { render json: @notification.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /notifications/1
      # PATCH/PUT /notifications/1.json
      def update
        respond_to do |format|
          if @notification.update(notification_params)
            format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
            format.json { render :show, status: :ok, location: @notification }
          else
            format.html { render :edit }
            format.json { render json: @notification.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /notifications/1
      # DELETE /notifications/1.json
      def destroy
        @notification.destroy
        respond_to do |format|
          format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_notification
        @notification = Notification.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def notification_params
        params.require(:notification).permit(:title, :description, :url, :article_id)
      end
    end
