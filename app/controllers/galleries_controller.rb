class GalleriesController < ApplicationController
  before_action :set_gallery, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :except => [:show]
  layout :resolve_layout
  # GET /galleries
  # GET /galleries.json
  def index
    @galleries = Gallery.all
  end

  # GET /galleries/1
  # GET /galleries/1.json
  def show
    prepare_meta_tags(
    title: @gallery.title,
    description: ActionView::Base.full_sanitizer.sanitize(@gallery.content).truncate(250),
    image: @gallery.cover,
    twitter: {
      site_name: "NOW JAKARTA | #{@gallery.title || @gallery.title}" ,
      site: '@NOW_Jakarta',
      creator: '@NOW_Jakarta',
      card: 'summary',
      description: ActionView::Base.full_sanitizer.sanitize(@gallery.content).truncate(250),
      title: @gallery.title,
      image: request.protocol + request.host_with_port + @gallery.cover.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @gallery.slug,
      site_name:  "NOW JAKARTA | #{@gallery.title || @gallery.title}" ,
      title: @gallery.title,
      image: request.protocol + request.host_with_port + @gallery.cover.url,
      type: 'gallery',
      description:   ActionView::Base.full_sanitizer.sanitize(@gallery.content).truncate(250)
      })
  end

  # GET /galleries/new
  def new
    @gallery = Gallery.new
    @gallery.image_sliders.build
  end

  # GET /galleries/1/edit
  def edit
    @gallery.image_sliders.build
  end

  # POST /galleries
  # POST /galleries.json
  def create

    @gallery = Gallery.new(gallery_params)

    respond_to do |format|
      if @gallery.save
        format.html { redirect_to @gallery, notice: 'Gallery was successfully created.' }
        format.json { render :show, status: :created, location: @gallery }
      else
        format.html { render :new }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /galleries/1
  # PATCH/PUT /galleries/1.json
  def update
    respond_to do |format|
      if @gallery.update(gallery_params)
        format.html { redirect_to @gallery, notice: 'Gallery was successfully updated.' }
        format.json { render :show, status: :ok, location: @gallery }
      else
        format.html { render :edit }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /galleries/1
  # DELETE /galleries/1.json
  def destroy
    @gallery.destroy
    respond_to do |format|
      format.html { redirect_to galleries_url, notice: 'Gallery was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    Gallery.import(params[:file])
    redirect_to galleries_path, notice: "Gallery imported."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gallery
      @gallery = Gallery.find(params[:id])
    end


      def add_more_images(new_images)
        images = @gallery.images
        images += new_images
        @gallery.images = images
      end

      def remove_image_at_index(index)
        remain_images = @gallery.images # copy the array
        deleted_image = remain_images.delete_at(index) # delete the target image
        deleted_image.try(:remove!) # delete image from S3
        @gallery.images = remain_images # re-assign back
      end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gallery_params
      params.require(:gallery).permit(:title, :cover, :content, :publish, :photographer, :slider_type,  image_sliders_attributes: [:image, :caption, :gallery_id, :_destroy])
    end

    def resolve_layout
    case action_name
    when 'show'
      'dark'
    else
      'admin'
    end
  end
end
