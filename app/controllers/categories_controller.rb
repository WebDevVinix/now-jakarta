class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :except => [:show, :detail]
  layout "admin", :except => [:show, :detail]
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @category.slug = params[:category_name]
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  def detail
    if request.fullpath.split("/")[2] == nil
      @category = Category.find(request.path.delete('/'))
      @first = Article.publish.publish.where.not(id: @first).in_group(@category.id).first
      @articles = Article.publish.publish.where.not(id: @first).in_group(@category.id).page(params[:page]).per(4)
    else
      if params[:page].present?
        @category = Category.find(request.fullpath.split("/")[2].split("?")[0])
      else
        @category = Category.find(request.fullpath.split("/")[2].split("?")[0])
      end

      if Category.magazine_issue.map(&:id).include?(@category.id)
        @articles = Article.publish.publish.where(category_id: @category.id).page(params[:page]).per(4)
      else
        @first = Article.publish.publish.where(category_id: Category.where(slug: @category.slug).first.id).first
        @articles = Article.publish.publish.where.not(id: @first).where(category_id: @category.id).page(params[:page]).per(4)
      end
    end

    if  @category.slug == 'chamber-updates'
      @show_cham = true
    else
      @show_cham = false
    end

    prepare_meta_tags(
      title: @category.title,
      description: @category.desc,
      image: request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
      twitter: {
        site_name: "NOW JAKARTA | #{@category.title}" ,
        site: '@NOW_Jakarta',
        creator: '@NOW_Jakarta',
        card: 'summary',
        description: @category.desc,
        title: @category.title,
        image: request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
      },
      og: {
        url: request.original_url,
        site_name: "NOW JAKARTA | #{@category.title}" ,
        title:  @category.title,
        image: request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
        type: 'article',
        description: @category.desc,
        })


        respond_to do |format|
          format.html
          format.js
        end
      end

      # GET /categories/1/edit
      def edit
      end

      # POST /categories
      # POST /categories.json
      def create
        @category = Category.new(category_params)

        respond_to do |format|
          if @category.save
            format.html { redirect_to @category, notice: 'Category was successfully created.' }
            format.json { render :show, status: :created, location: @category }
          else
            format.html { render :new }
            format.json { render json: @category.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /categories/1
      # PATCH/PUT /categories/1.json
      def update
        respond_to do |format|
          if @category.update(category_params)
            format.html { redirect_to categories_path, notice: 'Category was successfully updated.' }
            format.json { render :show, status: :ok, location: @category }
          else
            format.html { render :edit }
            format.json { render json: @category.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /categories/1
      # DELETE /categories/1.json
      def destroy
        @category.destroy
        respond_to do |format|
          format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      def import
        Category.import(params[:file])
        redirect_to categories_path, notice: "Article imported."
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_category
        @category = Category.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def category_params
        params.require(:category).permit(:title, :desc, :parent_id, :main, :slug, :cover, :month, :year, :emag_link)
      end
    end
