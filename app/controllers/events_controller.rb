class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :except => [:show, :detail]
  layout "admin", :except => [:show, :detail]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    if params[:start].present?
      @events = @events.where(created_at: DateTime.parse(params[:start]).beginning_of_day..DateTime.parse(params[:end]).end_of_day)
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show

    @categories = Category.event.children
    @months = Event.up_coming.order(end_date: :asc)
    @other_event =  Event.up_coming.where.not(id: @event.id).limit(5)

    prepare_meta_tags(
      title: @event.meta_title || @event.title,
      description: @event.meta_des || @event.content.html_safe.truncate(160),
      image: @event.image,
      twitter: {
        site_name: "NOW JAKARTA | #{@event.meta_des || @event.title}" ,
        site: '@NOW_Jakarta',
        creator: '@NOW_Jakarta',
        card: 'summary',
        description: @event.meta_des || ActionView::Base.full_sanitizer.sanitize(@event.content).truncate(250),
        title: @event.meta_title || @event.title,
        image: request.protocol + request.host_with_port + @event.image.url
      },
      og: {
        url: request.protocol + request.host_with_port+"/"+ @event.slug,
        site_name:  "NOW JAKARTA | #{@event.meta_des || @event.title}" ,
        title: @event.meta_title || @event.title,
        image: request.protocol + request.host_with_port + @event.image.url,
        type: 'event',
        description:  @event.meta_des || ActionView::Base.full_sanitizer.sanitize(@event.content).truncate(250)
        })

      end

      # GET /events/new
      def new
        @event = Event.new
      end

      # GET /events/1/edit
      def edit
      end

      # POST /events
      # POST /events.json
      def create
        @event = Event.new(event_params)

        respond_to do |format|
          if @event.save
            format.html { redirect_to @event, notice: 'Event was successfully created.' }
            format.json { render :show, status: :created, location: @event }
          else
            format.html { render :new }
            format.json { render json: @event.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /events/1
      # PATCH/PUT /events/1.json
      def update
        respond_to do |format|
          if @event.update(event_params)
            format.html { redirect_to @event, notice: 'Event was successfully updated.' }
            format.json { render :show, status: :ok, location: @event }
          else
            format.html { render :edit }
            format.json { render json: @event.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /events/1
      # DELETE /events/1.json
      def destroy
        @event.destroy
        respond_to do |format|
          format.html { redirect_to '/list-event', notice: 'Event was successfully destroyed.' }
          format.json { head :no_content }
        end
      end


      def import
        Event.import(params[:file])
        redirect_to events_path, notice: "Article imported."
      end

      def detail
        @categories = Category.event.children
        @months = Event.up_coming.order(start_date: :asc)
        if request.fullpath.split("/")[2].split("?")[0] == "education"
          xcat =  Category.find('education-853cabb7-3568-4c3f-b79e-1b578a14e672')
          @up_coming = Event.up_coming.where(category_id: xcat).order(start_date: :asc)
        elsif request.fullpath.split("/")[2].split("?")[0] == "festive"
          xcat =  Category.find('festive-40b19b1d-580e-494b-bec5-a3840a40e7a1')
          @up_coming = Event.up_coming.where(category_id: xcat).order(start_date: :asc)
        elsif request.fullpath.split("/")[2].split("?")[0] == "bazaar-and-festival"
          xcat =  Category.find('bazaar-festival')
          @up_coming = Event.up_coming.where(category_id:xcat ).order(start_date: :asc)
        else
          xcat = Category.find(request.fullpath.split("/")[2].split("?")[0])
          @up_coming = Event.up_coming.where(category_id: xcat).order(start_date: :asc)
        end

        if params[:month].present?
          @up_coming = @up_coming.by_month( params[:month], field: :start_date)
        end
        prepare_meta_tags(
          title: "EVENTS",
          description: "#{xcat.desc} Events in #{Date.today.strftime("%B %Y")}: #{@up_coming.map{|x| x.title }.join(', ') rescue ""} ",
          image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
          twitter: {
          site_name: "NOW JAKARTA | EVENTS" ,
          site: '@NOW_Jakarta',
          creator: '@NOW_Jakarta',
          card: 'summary',
          description: "#{xcat.desc}. Events in #{Date.today.strftime("%B %Y")}: #{@up_coming.map{|x| x.title }.join(', ') rescue ""} ",
          title: "EVENTS",
          image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
        },
        og: {
          url: request.protocol + request.host_with_port+"/events",
          site_name: "NOW JAKARTA | EVENTS" ,
          title: "EVENTS",
          image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
          type: 'article',
          description: "#{xcat.desc}. Events in #{Date.today.strftime("%B %Y")}: #{@up_coming.map{|x| x.title }.join(', ') rescue ""} ",
          })
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_event
        @event = Event.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def event_params
        params.require(:event).permit(:title, :meta_title, :meta_des, :content, :start_date, :end_date, :map, :vanue, :category_id, :image)
      end
    end
