class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy, :trash]
  before_action :authenticate_user!, :except => [:show]
  layout "admin", :except => [:show]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all_article.first(500)
    if params[:start].present?
      @articles = Article.where(publish: DateTime.parse(params[:start]).beginning_of_day..DateTime.parse(params[:end]).end_of_day)
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    if params[:category_name] == nil
      redirect_to dtl_article_path(@article.category.parent.slug, @article.category.slug, @article.slug), status: 301
    end

    if @article.category.parent == nil
      category_name = @article.category.slug
    else
      category_name = @article.category.parent.slug
      sub = @article.category.slug
    end
    # @article.increase_view!
    if params[:source].present?
      if params[:source] == "web-push"
       # @article.increase_counter!
      end
    end
   #  @article.increase_counter!
    @popular = Article.publish.popular.limit(5)
    @related = Article.publish.where.not(id: @article.id).where(category_id:  @article.category_id).limit(5)
    @recent = Article.publish.limit(5)
    if Article.where(content_marketing: true).where("articles.content_marketing_expired <= ?", DateTime.now).count < 1
      @suggested = Article.where(content_marketing: true)
        if @suggested.last.id == @article.id
          @suggested = Article.publish.featured
        end
    else
      @suggested = Article.publish.featured
    end
    prepare_meta_tags(
    title: @article.meta_title,
    description: @article.meta_des,
    image: @article.image,
    twitter: {
      site_name: "NOW JAKARTA | #{@article.meta_title || @article.title}" ,
      site: '@NOW_Jakarta',
      creator: '@NOW_Jakarta',
      card: 'summary',
      description:   @article.meta_des || ActionView::Base.full_sanitizer.sanitize(@article.content).truncate(250),
      title: @article.meta_title || @article.title,
      image: request.protocol + request.host_with_port + @article.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @article.slug,
      site_name:  "NOW JAKARTA | #{@article.meta_title || @article.title}" ,
      title: @article.meta_title || @article.title,
      image: request.protocol + request.host_with_port + @article.image.url,
      type: 'article',
      description:  @article.meta_des || ActionView::Base.full_sanitizer.sanitize(@article.content).truncate(250)
      })
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to edit_article_path(@article), notice: 'Article was successfully created.' }
        format.json { render :edit, status: :created, location: @article }
        if @article.status == "draft"
          NotifMailer.article(@article).deliver
        end
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    if params[:publish].present?
      @article.status = 'publish'
      respond_to do |format|
        if @article.update(article_params)
          format.html { redirect_to edit_article_path(@article), notice: 'Article was successfully updated.' }
          format.json { render :edit, status: :ok, location: @article }
        else
          format.html { render :edit }
          format.json { render json: @article.errors, status: :unprocessable_entity }
        end
      end
    else
      @article.status = 'draft'
      respond_to do |format|
        if @article.update(article_params)
          format.html { redirect_to edit_article_path(@article), notice: 'Article was successfully updated.' }
          format.json { render :edit, status: :ok, location: @article }
        else
          format.html { render :edit }
          format.json { render json: @article.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def trash
    respond_to do |format|
      if @article.update(:status => 'trash')
        format.html { redirect_to edit_article_path(@article), notice: 'Article was moved to trash.' }
        format.json { render :edit, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def import
    Article.import(params[:file])
    redirect_to articles_path, notice: "Article imported."
  end

  def export
    @articles = Article.first(50)
    if params[:start].present?
      @articles = Article.where(publish: DateTime.parse(params[:start]).beginning_of_day..DateTime.parse(params[:end]).end_of_day)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.

    def article_params
      params.require(:article).permit(:title, :meta_title, :meta_des, :content, :publish, :featured, :editor_pick, :image, :status, :category_id, :writer_id, :slug, :main_theme, :tag_list,:content_marketing,:content_marketing_expired, :slider, :gallery_id, :wpn_counter)
    end

end
