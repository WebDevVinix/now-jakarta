class PageController < ApplicationController

  layout "dark", :only => [:photos]

  def index
    @slider_homepage =  Slider.where(type_slider: "homepage").order(index: :asc)
    @soapbox = Article.publish.soapbox.order(publish: :desc).first
    @now_people = Article.publish.now_people.order(publish: :desc).first
    @featured = Article.publish.featured.order(publish: :desc).first
    @feature_second = Article.publish.featured.order(publish: :desc).second
    @magazine_issue = Category.magazine_issue.order(created_at: :asc).last
    @article_magazine_issue = Article.publish.where(category_id: @magazine_issue)
    @slider_magazine_issue = Slider.where(type_slider: "magazine_issue").order(index: :asc)
    if Article.where(content_marketing: true).where("articles.content_marketing_expired <= ?", DateTime.now).count < 1
      @artnculture = Article.where(content_marketing: true).last
    else
      @artnculture = Article.art_and_culture.first
    end
    @editors_pick = Article.editor_pick
    @travel =   Article.publish.where(category_id: Category.find('explore-jakarta')).limit(5)
    @dining = Article.publish.where(category_id: 278).limit(5)
    @special_offers = Article.publish.special_offers.limit(3)
    @events  =  Event.where("events.end_date >=?", Date.today).order("start_date ASC").limit(8)
    @video = Video.all.order(created_at: :desc)
    @photos = Gallery.where(slider_type: "Stories").order(publish: :desc)
    @contributor = Writer.where(main_contributor: 'true') #only contributor
    @latest = Article.publish.where.not(id: [@soapbox.id ,@now_people.id,@featured.id, @editors_pick.id ,@artnculture.id]).where.not(id: @travel.map(&:id)).where.not(id: @dining.map(&:id)).where.not(id:@special_offers.map(&:id)).order(publish: :desc)


  end

  def about
  end

  def advertise
  end

  def contact
    @contact = Contact.new
  end

  def events
    @categories = Category.event.children
    up_coming = Event.up_coming.map(&:id)
    on_going = Event.on_going.map(&:id)
    @months = Event.up_coming.order(start_date: :asc)
    @events = Event.where(id: (up_coming + on_going).uniq)
    if params[:category].present?
      @events = Event.up_coming.where(category_id: Category.where(slug: params[:category]).first.id).order(end_date: :asc)
    end
    if params[:month].present?
      a = Event.where("events.start_date >= ?", Date.parse("#{params[:month]} 1 2020").beginning_of_month).where("events.end_date <= ?", Date.parse("#{params[:month]} 1 2020").end_of_month)
      c = Event.where("events.end_date = ?", Date.parse("#{params[:month]} 1 2020").beginning_of_month)
      b = Event.where("events.start_date <= ?", Date.parse("#{params[:month]} 1 2020").beginning_of_month).where("events.end_date >= ?", Date.parse("#{params[:month]} 1 2020").end_of_month)
      @events = Event.where(id: (a+b+c).uniq).order(start_date: :asc)
    end
    prepare_meta_tags(
      title: "EVENTS",
      description: "NOW! Jakarta supports the best events in Jakarta including music concerts, festivals, bazaars, exhibitions, seminars, workshops & other trendy Jakarta events. Events in #{Date.today.strftime("%B %Y")}: #{@events.map{|x| x.title }.join(', ')} ",
      image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
      twitter: {
      site_name: "NOW JAKARTA | EVENTS" ,
      site: '@NOW_Jakarta',
      creator: '@NOW_Jakarta',
      card: 'summary',
      description: "NOW! Jakarta supports the best events in Jakarta including music concerts, festivals, bazaars, exhibitions, seminars, workshops & other trendy Jakarta events.",
      title: "EVENTS",
      image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
    },
    og: {
      url: request.protocol + request.host_with_port+"/events",
      site_name: "NOW JAKARTA | EVENTS" ,
      title: "EVENTS",
      image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
      type: 'article',
      description: "NOW! Jakarta supports the best events in Jakarta including music concerts, festivals, bazaars, exhibitions, seminars, workshops & other trendy Jakarta events.",
      })
  end

  def subscribe
    @subscribe = Subscribe.new
  end

  def photos
    @first = Gallery.where(slider_type: "Stories").order(publish: :desc).first
    @all = Gallery.where(slider_type: "Stories").where.not(id:@first.id).order(publish: :desc)
  end

  def latest
    @first = Article.publish.first
    @all = Article.publish.where.not(id: @first).page(params[:page]).per(5)
    @popular = Article.popular.limit(5)
    @recent = Article.publish.limit(5)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def city
    prepare_meta_tags(
      title: "City Guide",
      description: "NOW! Jakarta is the ultimate city guide for those who want to explore the best restaurants, cafes, hotels and landmarks in Jakarta.",
      image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
      twitter: {
      site_name: "NOW JAKARTA | City Guide" ,
      site: '@NOW_Jakarta',
      creator: '@NOW_Jakarta',
      card: 'summary',
      description: "NOW! Jakarta is the ultimate city guide for those who want to explore the best restaurants, cafes, hotels and landmarks in Jakarta.",
      title: "City Guide",
      image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
    },
    og: {
      url: request.protocol + request.host_with_port+"/events",
      site_name: "NOW JAKARTA | City Guide" ,
      title: "City Guide",
      image:  request.protocol + request.host_with_port + Category.magazine_issue.last.cover.url,
      type: 'article',
      description: "NOW! Jakarta is the ultimate city guide for those who want to explore the best restaurants, cafes, hotels and landmarks in Jakarta.",
      })
    @articles = Article.publish.limit(12)
  end

  def detail_area

  end

  def search
    @tagged = Article.tagged_with(params[:q]).map{|x| x.id}
    @query = Article.search(params[:q]).map{|x| x.id}
    @query |= @tagged
    @search_result = Article.where(id: @query)
  end

  def njsc_2019_winner

  end

  def tagged
    @taggeds = Article.tagged_with(params[:q]).limit(1)
  end
end
