class BannersController < ApplicationController
  before_action :set_banner, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :except => [:show, :counter]
  layout "admin", :except => [:show, :counter]
  # GET /banners
  # GET /banners.json
  def index
    @banners = Banner.all
    @checkpop = Banner.where(position: 'pop').where(status: 'active').first
    @checkctrtop = Banner.where(position: 'ctrtop').where(status: 'active').first
    @checkctrleft = Banner.where(position: 'ctrleft').where(status: 'active').first
    @checkctrright = Banner.where(position: 'ctrright').where(status: 'active').first
    @checkhp1 = Banner.where(position: 'hp1').where(status: 'active').first
    @checkhp2 = Banner.where(position: 'hp2').where(status: 'active').first
    @checkhp3 = Banner.where(position: 'hp3').where(status: 'active').first
    @checkhp3m = Banner.where(position: 'hp3m').where(status: 'active').first
    @checkhp4 = Banner.where(position: 'hp4').where(status: 'active').first
    @checkhp4m = Banner.where(position: 'hp4m').where(status: 'active').first
    @checkhp5 = Banner.where(position: 'hp5').where(status: 'active').first
    @checkhp5m = Banner.where(position: 'hp5m').where(status: 'active').first
    @checkhp6 = Banner.where(position: 'hp6').where(status: 'active').first
    @checkhp6m = Banner.where(position: 'hp6m').where(status: 'active').first
    @checkhp7 = Banner.where(position: 'hp7').where(status: 'active').first
    @checkhp7m = Banner.where(position: 'hp7m').where(status: 'active').first
    @checkhp8 = Banner.where(position: 'hp8').where(status: 'active').first
    @checkhp8l = Banner.where(position: 'hp8l').where(status: 'active').first
    @checkhp8lm = Banner.where(position: 'hp8lm').where(status: 'active').first
    @checkhp9 = Banner.where(position: 'hp9').where(status: 'active').first
    @checkhp9l = Banner.where(position: 'hp9l').where(status: 'active').first
    @checkhp9lm = Banner.where(position: 'hp9lm').where(status: 'active').first
    @checkhp10 = Banner.where(position: 'hp10').where(status: 'active').first
    @checkhp11 = Banner.where(position: 'hp11').where(status: 'active').first
    @checkhp10l = Banner.where(position: 'hp10l').where(status: 'active').first
    @checkhp10lm = Banner.where(position: 'hp10lm').where(status: 'active').first
  end

  # GET /banners/1
  # GET /banners/1.json
  def show
  end

  # GET /banners/new
  def new
    @banner = Banner.new
  end

  # GET /banners/1/edit
  def edit
  end

  # POST /banners
  # POST /banners.json
  def create
    unless params[:banner][:position].nil? || params[:banner][:position].blank?
      @banner = Banner.new(banner_params)

      respond_to do |format|
        @banner.status = 'active'
        if @banner.save
          format.html { redirect_to edit_banner_path(@banner.id), notice: 'Banner was successfully created.' }
          format.json { render :show, status: :created, location: @banner }
        else
          format.html { render :new }
          format.json { render json: @banner.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to banners_path, alert: 'Invalid Parameters' }
        format.json { render :index, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /banners/1
  # PATCH/PUT /banners/1.json
  def update
    respond_to do |format|
      if @banner.update(banner_params)
        format.html { redirect_to edit_banner_path(@banner.id), notice: 'Banner was successfully updated.' }
        format.json { render :show, status: :ok, location: @banner }
      else
        format.html { render :edit }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /banners/1
  # DELETE /banners/1.json
  def destroy
    @banner.destroy
    respond_to do |format|
      format.html { redirect_to banners_url, notice: 'Banner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def editbanner
    unless params[:banner][:position].nil? || params[:banner][:position].blank? || params[:banner][:id].nil? || params[:banner][:id].blank?
      @banner = Banner.where(id: params[:banner][:id])
      if @banner.update(banner_params)
        respond_to do |format|
          format.html { redirect_to request.env["HTTP_REFERER"], notice: 'Banner was successfully updated.' }
          format.json { render :show, status: :ok, location: @banner }
        end
      else
        respond_to do |format|
          format.html { redirect_to request.env["HTTP_REFERER"] }
          format.json { render json: @banner.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to banners_path, alert: 'Invalid Parameters' }
        format.json { render :index, status: :unprocessable_entity }
      end
    end
  end

  def remover
    if params[:id].nil? || params[:id].blank?
      respond_to do |format|
        format.html { redirect_to banners_path, alert: 'Invalid Parameters' }
        format.json { render :index, status: :unprocessable_entity }
      end
    else
      @banner = Banner.find(params[:id])
      if @banner.update_attribute(:status, 'trash')
        respond_to do |format|
          format.html { redirect_to request.env["HTTP_REFERER"], notice: 'Banner was successfully remove.' }
          format.json { render :show, status: :ok, location: @banner }
        end
      else
        respond_to do |format|
          format.html { redirect_to request.env["HTTP_REFERER"] }
          format.json { render json: @banner.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def active
    # code
  end

  def trash
    @banners = Banner.where(status: 'trash')
  end

  def counter
    banner = Banner.find(params[:id])
    banner.update_attribute(:click, banner.click.to_i + 1)
    redirect_to banner.link
  end

  def send_report_banner
    banner = Banner.find(params[:id])
    banner.update_attribute(:status, "trash")
    NotifMailer.send_report_banner(banner).deliver
    redirect_to banners_article_path, notice: 'Report Send'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_banner
      @banner = Banner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def banner_params
      params.require(:banner).permit(:title, :image, :start_date, :expired_date, :link, :position, :click, :widget, :status, :category_id, :screen_shoot)
    end
end
