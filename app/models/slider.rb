class Slider < ApplicationRecord
  mount_uploader :image, SliderUploader
  belongs_to :article, optional: true
end
