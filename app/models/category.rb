class Category < ApplicationRecord
  extend FriendlyId
  mount_uploader :cover, CoverUploader
  friendly_id :title

  has_many :articles
  has_many :events
  has_many :banners

  has_many :children, :class_name => "Category", :foreign_key => :parent_id
  belongs_to :parent, :class_name => "Category", :foreign_key => :parent_id, optional: true



  class << self
    def event
      Category.where(slug: "events-article").first
    end

    def soapbox
      Category.where(slug: "soapbox").first
    end

    def people
      Category.where(slug: "people").first
    end

    def now_people
      Category.where(slug: "now-people").first
    end

    def updates
      Category.where(slug: "updates")
    end

    def dining
      Category.where(slug: "dining").first
    end

    def travel
      Category.where(slug: "travel").first
    end

    def art_and_culture
      Category.where(slug: "art-and-culture").first
    end

    def special_offers
      Category.where(slug: "special-offers").first.children
    end

    def kidsandfamily
      Category.where(slug: "kids-family")
    end

    def lifestyle
        Category.where(slug: "lifestyle")
    end

    def business
      Category.where(slug: "business")
    end

    def magazine_issue
      Category.find("magazine-issue").children
    end

    def home_life
      Category.where(slug: "home-life")
    end

    def parent
      Category.where(parent_id: [nil,""])
    end

    def events
      Category.find(Category.find('events-article').id).children
    end

    def for_articles
      Category.where.not(id: find(Category.find('events-article').id).children.map(&:id)).where.not(id: Category.parent.map(&:id))
    end


  end


  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      if Category.exists?(row["id"])
      else
      new
      params =
      {category:
        {id: row["id"],
          title: row["title"],
          desc: row["desc"],
          parent_id: row["parent_id"]
        }
       }
        Category.create(params[:category])
      end
      end
    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
end
