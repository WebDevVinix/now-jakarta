class ImageSlider < ApplicationRecord
  belongs_to :gallery
  mount_uploader :image, ImagesGalleryUploader

end
