class Gallery < ApplicationRecord
  extend FriendlyId
  serialize :images
  mount_uploader :cover, CoverUploader
  mount_uploaders :images, ImagesGalleryUploader
  friendly_id :title
  has_many :image_sliders, dependent: :delete_all
  accepts_nested_attributes_for :image_sliders, reject_if: :all_blank, allow_destroy: true

  Photographer = [ "Raditya Fadilla", "Sasha Muldya", "Basuki Nugroho","Stephanie Brookes","David Metcalf","NOW! Jakarta" ]
  Type = [ "Stories", "Article" ]

  class << self
    def article
      Gallery.where(slider_type:"Article")
    end
  end


  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |y|

      row = Hash[[header, spreadsheet.row(y)].transpose]
      a = []
      row["count"].to_i.times{ |x| a << row["images/#{x}"] }
      if Gallery.exists?(row["id"])
      else
      new
      params =
      {gallery:
        {
          id: row["id"],
          slug: row["slug"],
          title:  row["title"],
          remote_cover_url: row["cover"],
          remote_images_urls: a,
          content: row["content"],
          publish: row["publish"],
        }
       }
        Gallery.create(params[:gallery])
      end
    end
    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
end
