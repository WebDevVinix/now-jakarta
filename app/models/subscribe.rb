class Subscribe < ApplicationRecord

  def read!
    self.status = "read"
    self.save
  end

end
