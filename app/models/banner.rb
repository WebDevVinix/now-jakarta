class Banner < ApplicationRecord
  belongs_to :category, optional: true

  mount_uploader :image, BannerUploader
  mount_uploader :screen_shoot, BannerUploader

class << self
  def running
    Banner.where("banners.expired_date >= ?", Date.today)
  end

  def avaiable_to_create_report
    Banner.where("banners.expired_date = ?", Date.yesterday)
  end
end


  def expired?
    self.expired_date > Date.today
  end


  STATUS = [ :active, :trash ]

  POSITION = {hp1:  'Homepage Square Feature 1',
              hp2:  'Homepage Square Feature 2',
              hp3:  'Homepage Long Feature 1',
              hp3m:  'Homepage Long Feature 1 Mobile',
              hp4:  'Homepage Long Feature 2',
              hp4m:  'Homepage Long Feature 2 Mobile',
              hp5:  'Homepage Long Latest',
              hp5m:  'Homepage Long Latest Mobile',
              hp6:  'Homepage Long Magazine Issue',
              hp6m:  'Homepage Long Magazine Issue Mobile',
              hp7:  'Homepage Long Event',
              hp7m:  'Homepage Long Event Mobile',
              hp8l:  'Homepage Long Dine and Travel',
              hp8lm:  'Homepage Long Dine and Travel Mobile',
              hp8:  'Homepage Square Travel',
              hp9:  'Homepage Square Dining',
              hp10:  'Homepage Square Promotion',
              hp11:  'Homepage Square Other',
              hp9l:  'Homepage Long Video',
              hp9lm:  'Homepage Homepage Long Video Mobile',
              hp10l:  'Homepage Long Deals',
              hp10lm:  'Homepage Long Deals Mobile',
              pop: 'Homepage popup',
              ctrtop: 'page category top',
              ctrleft: 'page category left',
              ctrright: 'page category right'
            }



end
