class Event < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  mount_uploader :image, EventUploader

  belongs_to :category

  class << self
    def up_coming
      Event.where("events.start_date >= ?", DateTime.now)
    end
    def on_going
      Event.where("events.end_date >= ?", DateTime.now)
    end
  end



  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      new
      params =
      {event:
        {
          title: row["title"],
          meta_title: row["meta_title"],
          content: row["content"],
          slug:  row["slug"],
          start_date:  row["start"],
          end_date:  row["end"],
          map:  row["map"],
          vanue:  row["location"],
          remote_image_url: row["image"],
          category_id:  row["category_id"]
        }
       }
        Event.create(params[:event])
      end
    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
end
