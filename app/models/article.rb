class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  acts_as_taggable_on :tags

  mount_uploader :image, ImageUploader

  belongs_to :writer
  belongs_to :category
  belongs_to :gallery, optional: :true

  Status = [ :publish, :draft, :trash ]

  default_scope { order(publish: :desc) }

  validates :title, presence: true

  class << self

    def on_scheduled
      Article.where("articles.publish >= ?", DateTime.now)
    end

    def publish
      Article.where(status: "publish").where("articles.publish <= ?", DateTime.now).order(publish: :desc)
    end

    def drafted
      Article.where(status: "draft")
    end

    def all_article
        Article.where(status: ["publish","draft"])
    end

    def popular
      Article.where.not(status: 'trash').where.not(status: 'draft').where('publish > ?',30.days.ago).reorder(view_count: :desc)
    end

    def soapbox
      Article.publish.where(category_id: Category.soapbox.id)
    end

    def now_people
      Article.publish.where(category_id: Category.now_people)
    end

    def main_theme
      Article.publish.where(main_theme: true)
    end

    def editor_pick
      Article.publish.where(editor_pick: true).first
    end

    def featured
      Article.publish.where(featured: true)
    end

    def travel
      Article.publish.where(category_id: Category.only_travel)
    end

    def dining
      Article.publish.where(category_id: Category.only_dining)
    end

    def special_offers
      Article.publish.where(category_id: Category.special_offers)
    end

    def art_and_culture
      Article.publish.where(category_id: Category.art_and_culture.children.map(&:id))
    end

    def home_life
      Article.publish.where(category_id: Category.home_life.children.map(&:id))
    end
  end

  def self.search(search)
    where("title ILIKE ? OR meta_des ILIKE ?", "%#{search}%", "%#{search}%")
  end

  def self.in_group(category)
      publish.where(category_id:[ Category.find(category).children.map(&:id)])
  end

  def increase_view!
    self.view_count = self.view_count.to_i + 1
    self.save
  end

  def increase_counter!
    self.wpn_counter = self.wpn_counter.to_i + 1
    self.save
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      if Article.exists?(row["id"])
      else
      new
      params =
      {article:
        {
          id: row["id"],
          title: row["title"],
          meta_title: row["meta_title"],
          meta_des: row["meta_des"],
          content: row["content"],
          publish: row["publish"],
          featured: row["featured"],
          editor_pick: row["editor_pick"],
          category_id: row["category_id"],
          remote_image_url: row["image"],
          slug: row["slug"],
          writer_id: row["writer_id"],
          status: "publish"
        }
       }
        Article.create(params[:article])
      end
      end


    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end

end
