class Writer < ApplicationRecord
extend FriendlyId
friendly_id :fullname

mount_uploader :image, ImageUploader

class << self
  def main_contributor
    Writer.where(main_contributor: true)
  end
end

has_many :articles, dependent: :destroy
  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      if Writer.exists?(row["id"])
      else
       new
      params =
      {writer:
        {id: row["id"],
          fullname: row["fullname"],
          desc: row["desc"],
          main_contributor: row["main"],
          remote_image_url: row["image"],
          slug: row["slug"],
        }
       }
        Writer.create(params[:writer])
      end
    end
    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
end
