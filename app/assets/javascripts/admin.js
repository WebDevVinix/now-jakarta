// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require activestorage
// require turbolinks
//= require ckeditor/loader
//= require moment
//= require public/bootstrap.min
//= require dataTables/jquery.dataTables
//= require admin/popper.min
//= require admin/dragula.min
//= require admin/app-style-switcher
//= require admin/app.init
//= require admin/app
//= require admin/perfect-scrollbar.jquery.min
//= require admin/sidebarmenu
//= require admin/waves
//= require admin/datatables.min
//= require admin/bootstrap-switch.min
//= require admin/sparkline
//= require admin/jquery.sortable
//= require admin/dropify
//= require admin/bootstrap-material-datetimepicker
//= require select2
//= require cocoon
//= require selectize
//= require admin/custom
//= require toastr

// global toastr
toastr.options = {
  "closeButton": false,
  "debug": false,
  "positionClass": "toast-bottom-left",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
