// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery
//= require rails-ujs
//= require activestorage

//= require ckeditor/init
//= require moment
//= require bootstrap-datetimepicker
//= require public/bootstrap.min
//= require public/morphext
//= require public/owl.carousel.min
//= require public/jquery.mCustomScrollbar
//= require public/socialfeed
//= require toastr
//= require jquery.infinite-pages
//= require public/openweather.min
//= require public/lightbox
//= require public/main
//= require serviceworker
//= require serviceworker-companion
// global toastr
toastr.options = {
  "closeButton": false,
  "debug": false,
  "positionClass": "toast-bottom-left",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
