$( document ).ready(function() {


  $('.home-slider').owlCarousel({
    autoplayHoverPause:false,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    loop:true,
    dots: true,

    responsive:{
      0:{
        autoWidth:false,
        mouseDrag: false,
        items:1,
        nav:false
      },
      600:{
        nav:false,
        items:1
      },
      1200:{
        nav:false,
        items:1
      }
    }

  })

  $('.event-slider').owlCarousel({
    margin:10,
    autoplayTimeout:6000,
    responsive:{
      0:{
        mouseDrag: false,
        items:1,
        loop:true,
        dots: true,
        nav:false,
      },
      600:{
        items:3
      },
      1000:{
        nav:false,
        items:4
      }
    }
  })

  $('.deal-slider').owlCarousel({
    loop:false,
    margin:10,
    autoplayTimeout:6000,
    responsive:{
      0:{
        mouseDrag: false,
        items:1,
        loop:true,
        dots: true,
        nav:false,
      },
      600:{
        items:3
      },
      1000:{
        nav:true,
        items:4
      }
    }
  })


  $('.contributor-slider').owlCarousel({
    loop:true,
    margin:10,
    dots:false,
    autoplayTimeout:6000,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:3
      },
      1000:{
        items:3
      }
    }
  })
  $('#magazine-issue').carousel({
    interval: false
  })
  $('.first-article').matchHeight();


  $(".list-video").mCustomScrollbar();

  $('.photo-frame').matchHeight();
  $('.list-section').matchHeight();
  $('.sm-hgt-sldr').matchHeight();

  $( "#search-triger").click(function() {
    $("#search-bar").removeClass('d-none');
    $("#navbar").removeClass('opacity-1');
    $("#navbar").addClass('opacity-0');
    $("#white-layer").removeClass('d-none');
    $("#search_field").focus();
  });

  $( "#form-close").click(function() {
    $("#white-layer").addClass('d-none');
    $("#search-bar").addClass('d-none');
    $("#navbar").addClass('opacity-1');
    $("#navbar").removeClass('opacity-0');
  });
});



$('#all-feed').socialfeed({

  instagram:{
    accounts: ['@now_jakarta'],  //Array: Specify a list of accounts from which to pull posts
    limit: 12,                                    //Integer: max number of posts to load
    client_id: 'f771dcc38d634e349d30af3df6fc24a1',
    access_token: '1538402401.f771dcc.4473c20db8ea4cafb1b8704cd9d18e77'       //String: Instagram client id (optional if using access token)
  },

  // GENERAL SETTINGS
  length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
  show_media:true,                                //Boolean: if false, doesn't display any post images
  media_min_width: 300,                           //Integer: Only get posts with images larger than this value
  //Integer: Number of seconds before social-feed will attempt to load new posts.

  template_html:
  '<div class="col-md-2 mb-5"> \
  <a href="{{=it.link}}" target="_blank"> \
  <div class=""> \
  {{=it.attachment}} \
  </div> \
  </a> \
  </div>'                           //String: HTML used for each post. This overrides the 'template' filename option
});


$(function () {
  $(document).scroll(function () {
    var $nav = $("#article-title");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height() +15);
    $('#article-detail .category-before').toggleClass('invisible', $(this).scrollTop() > $nav.height() +15);
  });
  $(document).scroll(function () {
    var $nav = $("#small-logo");
    $nav.toggleClass('visible', $(this).scrollTop() > $nav.height());
  });
});

$(function() {
  $('.search-frm').each(function() {
    $(this).find('input').keypress(function(e) {
      // Enter pressed?
      if(e.which == 10 || e.which == 13) {
        this.form.submit();
      }
    });

    $(this).find('input[type=submit]').hide();
  });
});


$(function() {
  return $('.infinite-table').infinitePages({
    loading: function() {
      return $(this).text('Loading next page...');
    },
    error: function() {
      return $(this).button('There was an error, please try again');
    }
  });
});


$(".video-0,.video-1, .video-2, .video-3, .video-4, .video-5, .video-6, .video-7, .video-8").on("click", function(event) {
  event.preventDefault();
  $(".video_case iframe").prop("src", $(event.currentTarget).attr("href"));
});


setTimeout(function() {
    $('#enews').modal();
}, 15000);

jQuery(document.documentElement).keyup(function (event) {

    var owl = jQuery(".owl-carousel");

    // handle cursor keys
    if (event.keyCode == 37) {
       // go left
       owl.trigger('owl.prev');
    } else if (event.keyCode == 39) {
       // go right
       owl.trigger('owl.next');
    }

});

$(document).ready(function() {
  $('.meow-slider').owlCarousel({
    autoplay:true,
    autoplayTimeout:6000,
    autoplayHoverPause:false,
    center:true,

    nav:false,
    loop:true,
    URLhashListener:true,
    dots: false,
    lazyLoad:true,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:3
      },
      1000:{
        nav:true,
        items:3
      }
    }
  })

  $('.slider-thumb').owlCarousel({
    loop:true,
    margin:10,
    autoplayTimeout:6000,
    nav:true,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:3
      },
      1000:{
        nav:true,
        items:6
      }
    }
  })
})

// // $(".popupbanner").fancybox();
// $(document).on('click', 'a[href^="#"]', function(e) {
//        // target element id
//        var id = $(this).attr('href');
//
//        // target element
//        var $id = $(id);
//        if ($id.length === 0) {
//            return;
//        }
//
//        // prevent standard hash navigation (avoid blinking in IE)
//        e.preventDefault();
//
//        // top position relative to the document
//        var pos = $id.offset().top - 20;
//
//        // animated top scrolling
//        $('body, html').animate({scrollTop: pos});
//    });

$("#selectMonth" ).change(function(){
  $("#selectCategory").val("");
  $('#filter-category').submit();
});

// $("#selectCategory" ).change(function(){
//   $("#selectMonth").val("");
//   $('#filter-category').submit();
// });

$('.articles a').not($('a[href^="mailto:"]')).attr('target','_blank');
$('.article a').not($('a[href^="mailto:"]')).attr('target','_blank');

 // $(function() {
 //   setTimeout(function() {
 //     $('#popupbanner').modal();
 //   }, 15000);
 // });


function addCopyrightInfo() {
    //Get the selected text and append the extra info
    var selection, selectedNode, html;
    if (window.getSelection) {
        var selection = window.getSelection();
        if (selection.rangeCount) {
            selectedNode = selection.getRangeAt(0).startContainer.parentNode;
            var container = document.createElement("div");
            container.appendChild(selection.getRangeAt(0).cloneContents());
            html = container.innerHTML;
        }
    }
    else {
        console.debug("The text [selection] not found.")
        return;
    }

    // Save current selection to resore it back later.
    var range = selection.getRangeAt(0);

    if (!html)
        html = '' + selection;

    html += "<br/><br/><small><span>Thank you for reading! Read many other interesting stories on nowjakarta.co.id. Source: </span><a target='_blank' title='" + document.title + "' href='" + document.location.href + "'>" + document.title + "</a></small><br/>";
    var newdiv = document.createElement('div');

    //hide the newly created container
    newdiv.style.position = 'absolute';
    newdiv.style.left = '-99999px';

    // Insert the container, fill it with the extended text, and define the new selection.
    selectedNode.appendChild(newdiv); // *For the Microsoft Edge browser so that the page wouldn't scroll to the bottom.

    newdiv.innerHTML = html;
    selection.selectAllChildren(newdiv);

    window.setTimeout(function () {
        selectedNode.removeChild(newdiv);
        selection.removeAllRanges();
        selection.addRange(range); // Restore original selection.
    }, 5); // Timeout is reduced to 10 msc for Microsoft Edge's sake so that it does not blink very noticeably.
}

document.addEventListener('copy', addCopyrightInfo);
