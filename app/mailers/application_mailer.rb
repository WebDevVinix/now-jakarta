class ApplicationMailer < ActionMailer::Base
  default from: "Phoenix Communications <no-reply@phoenix.co.id>"
  layout 'mailer'
end
