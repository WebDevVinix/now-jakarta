class NotifMailer < ApplicationMailer
  helper :application
  default from: "Digital Team NOW! Jakarta <no-reply@phoenix.co.id>"

  def article(article)
   @article = article
   mail( :to => ['syjaksilversong@gmail.com'],
     :subject => "New Article #{article.title}",
     :display_name => "NOW! Jakarta" )
  end

  def contact(contact)
   @contact = contact
   mail( :to => ['digital@phoenix.co.id'],
     :subject => "New Contact -#{contact.subject}",
     :display_name => "NOW! Jakarta" )
  end

  def subscribe(subscribe)
   @subscribe = subscribe
   mail( :to => ['digital@phoenix.co.id'],
     :subject => "New Subscribe",
     :display_name => "NOW! Jakarta" )
  end

  def send_report_banner(banner)
    @banner = banner
    mail( :to => ['digital@phoenix.co.id'],
      :subject => "Report Banner #{banner.title}",
      :display_name => "Digital Team NOW! Jakarta" )
  end

end
