if ($(window).width() > 1200) {
  $(window).scroll(function () {
    if($(window).scrollTop() > $('#header').offset().top) {
      $('#navbar-brand').removeClass('w-100');
      $('#navbar-brand').addClass("w-20");
      $('#logo-position').addClass("d-flex");
      $('#logo-position').addClass("justify-content-center");
      $('#header').addClass('navbar-sticky');
      $('#navbarSupportedContent').addClass ('d-in-block');
      $('#navbarSupportedContent').addClass ('w-75');
      $('.navbar-brand img').removeClass ('logo-big');


    }
    if($(window).scrollTop() <= $('body').offset().top) {
      $('#navbar-brand').addClass("w-100");
      $('#navbar-brand').removeClass('w-20');
      $('#logo-position').removeClass("d-flex");
      $('#logo-position').removeClass("justify-content-center");
      $('#header').removeClass ('navbar-sticky');
      $('#navbarSupportedContent').removeClass ('d-in-block');
      $('#navbarSupportedContent').removeClass ('w-75');
      $('.navbar-brand img').addClass ('logo-big');
    }
  });
}


h=$(window).height() - 131;
$('.bgitem').css({'height':h+'px'});
$('.home-slider').owlCarousel({
  autoplay:true,
  autoplayTimeout:6000,
  autoplayHoverPause:false,
  items:1,
  nav:false,
  loop:true,
  animateOut: 'fadeOut',
  dots: false
})
$('.gallery-slider').owlCarousel({
  autoplay:true,
  autoplayTimeout:6000,
  autoplayHoverPause:false,
  items:1,
  nav:false,
  loop:true,
  animateOut: 'fadeOut',
  nav:true,
  URLhashListener:true,
})

$('.slider-thumb').owlCarousel({
    loop:true,
    margin:10,
    autoplayTimeout:6000,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            nav:true,
            items:5
        }
    }
})


  $(window).on("load",function(){
    $(".list-video").mCustomScrollbar();
  });


  $( "#search-triger").click(function() {
      $("#search-bar").removeClass('d-none');
      $("#navbar").removeClass('opacity-1');
      $("#navbar").addClass('opacity-0');
  });

  $( "#form-close").click(function() {
    $("#search-bar").addClass('d-none');
    $("#navbar").addClass('opacity-1');
    $("#navbar").removeClass('opacity-0');
  });
