class AddArticleToNotification < ActiveRecord::Migration[5.2]
  def change
    add_reference :notifications, :article, foreign_key: true
  end
end
