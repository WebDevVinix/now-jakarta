class CreateGalleries < ActiveRecord::Migration[5.2]
  def change
    create_table :galleries do |t|
      t.string :cover
      t.string :content
      t.datetime :publish

      t.timestamps
    end
  end
end
