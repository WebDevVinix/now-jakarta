class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :title
      t.text :desc
      t.integer :parent_id
      t.string :main
      t.string :slug

      t.timestamps
    end
  end
end
