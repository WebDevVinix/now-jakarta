class CreateSubscribes < ActiveRecord::Migration[5.2]
  def change
    create_table :subscribes do |t|
      t.string :title
      t.string :fullname
      t.string :telephone
      t.string :phonenumber
      t.string :fax
      t.string :email
      t.text :address
      t.string :plan

      t.timestamps
    end
  end
end
