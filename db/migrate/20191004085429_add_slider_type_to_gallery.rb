class AddSliderTypeToGallery < ActiveRecord::Migration[5.2]
  def change
    add_column :galleries, :slider_type, :string
  end
end
