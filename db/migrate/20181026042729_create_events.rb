class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :meta_title
      t.string :meta_des
      t.text :content
      t.date :start_date
      t.date :end_date
      t.string :map
      t.string :vanue

      t.timestamps
    end
  end
end
