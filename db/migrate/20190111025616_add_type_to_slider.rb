class AddTypeToSlider < ActiveRecord::Migration[5.2]
  def change
    add_column :sliders, :type, :string
  end
end
