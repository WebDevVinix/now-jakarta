class AddSliderToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :slider, :boolean
    add_reference :articles, :gallery, foreign_key: true
  end
end
