class CreateImageSliders < ActiveRecord::Migration[5.2]
  def change
    create_table :image_sliders do |t|
      t.string :image
      t.string :caption
      t.references :gallery, foreign_key: true

      t.timestamps
    end
  end
end
