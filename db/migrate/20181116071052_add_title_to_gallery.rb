class AddTitleToGallery < ActiveRecord::Migration[5.2]
  def change
    add_column :galleries, :title, :string
  end
end
