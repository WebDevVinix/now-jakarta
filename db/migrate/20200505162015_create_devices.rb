class CreateDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :devices do |t|
      t.string :endpoint
      t.string :p256dh_key
      t.string :auth_key

      t.timestamps
    end
  end
end
