class AddTypeSliderToSlider < ActiveRecord::Migration[5.2]
  def change
    add_column :sliders, :type_slider, :string
  end
end
