class AddPhotographerToGallery < ActiveRecord::Migration[5.2]
  def change
    add_column :galleries, :photographer, :string
  end
end
