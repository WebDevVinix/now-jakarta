class CreateBanners < ActiveRecord::Migration[5.2]
  def change
    create_table :banners do |t|
      t.string :title
      t.string :image
      t.date :start_date
      t.date :expired_date
      t.string :link
      t.string :position
      t.integer :click
      t.text :widget
      t.string :status
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
