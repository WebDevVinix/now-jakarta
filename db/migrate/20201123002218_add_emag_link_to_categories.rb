class AddEmagLinkToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :emag_link, :string
  end
end
