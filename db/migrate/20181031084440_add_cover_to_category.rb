class AddCoverToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :cover, :string
  end
end
