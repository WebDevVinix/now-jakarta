class AddMainThemeToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :main_theme, :boolean
  end
end
