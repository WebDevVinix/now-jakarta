class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :meta_title
      t.text :meta_des
      t.text :content
      t.datetime :publish
      t.boolean :featured
      t.boolean :editor_pick
      t.string :image
      t.string :status
      t.references :category, foreign_key: true
      t.references :writer, foreign_key: true
      t.string :slug

      t.timestamps
    end
  end
end
