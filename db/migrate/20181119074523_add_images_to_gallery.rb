class AddImagesToGallery < ActiveRecord::Migration[5.2]
  def change
    add_column :galleries, :images, :string
  end
  # def change for prosgree
  #   add_column :galleries, :images, :string, array: true, default: []
  # end
end
