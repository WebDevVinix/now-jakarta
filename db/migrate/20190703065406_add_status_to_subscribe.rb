class AddStatusToSubscribe < ActiveRecord::Migration[5.2]
  def change
    add_column :subscribes, :status, :string
  end
end
