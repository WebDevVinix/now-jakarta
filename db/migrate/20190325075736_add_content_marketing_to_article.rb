class AddContentMarketingToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :content_marketing, :boolean
    add_column :articles, :content_marketing_expired, :date
  end
end
