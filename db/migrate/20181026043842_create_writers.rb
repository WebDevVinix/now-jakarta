class CreateWriters < ActiveRecord::Migration[5.2]
  def change
    create_table :writers do |t|
      t.string :fullname
      t.string :desc
      t.string :main_contributor
      t.string :image
      t.string :slug

      t.timestamps
    end
  end
end
