class AddYearToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :year, :string
  end
end
