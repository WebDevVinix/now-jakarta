class CreateDirectories < ActiveRecord::Migration[5.2]
  def change
    create_table :directories do |t|
      t.string :building_name
      t.string :street
      t.string :region
      t.string :city
      t.string :postal_code
      t.string :phone
      t.string :website
      t.string :user_ig
      t.string :link_ig
      t.string :user_facebook
      t.string :facebook_link
      t.string :email
      t.text :map
      t.string :lat
      t.string :long
      t.string :image
      t.string :venue_type

      t.timestamps
    end
  end
end
