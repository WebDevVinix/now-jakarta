class CreateJoinTableArticlesBanners < ActiveRecord::Migration[5.2]
  def change
    create_join_table :articles, :banners do |t|
      # t.index [:article_id, :banner_id]
      # t.index [:banner_id, :article_id]
    end
  end
end
