class CreateSliders < ActiveRecord::Migration[5.2]
  def change
    create_table :sliders do |t|
      t.references :article, foreign_key: true
      t.string :title
      t.string :image
      t.string :index

      t.timestamps
    end
  end
end
