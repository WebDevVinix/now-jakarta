class AddNameToDirectory < ActiveRecord::Migration[5.2]
  def change
    add_column :directories, :name, :string
  end
end
