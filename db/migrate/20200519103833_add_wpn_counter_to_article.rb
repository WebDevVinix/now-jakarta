class AddWpnCounterToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :wpn_counter, :integer
  end
end
