# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://nowjakarta.co.id/"

SitemapGenerator::Sitemap.create do
  add root_path, :changefreq => 'daily'
  add latest_path, :changefreq => 'daily'
  add all_event_path, :changefreq => 'daily'
  add photos_path, :changefreq => 'daily'
  add dining_path, :changefreq => 'daily'
  add people_path, :changefreq => 'daily'
  add art_and_culture_path, :changefreq => 'daily'
  add lifestyle_path, :changefreq => 'daily'
  add updates_path, :changefreq => 'daily'
  add nearest_event_path, :changefreq => 'daily'
  add business_path, :changefreq => 'daily'
  add travel_path, :changefreq => 'daily'
  add deals_path, :changefreq => 'daily'
  add themes_articles_path, :changefreq => 'daily'
  add allpost_path, :changefreq => 'daily'
  add soapbox_path, :changefreq => 'daily'
  add commitment_path, :changefreq => 'daily'
  add contest_path, :changefreq => 'daily'
  add subscribe_now_path, :changefreq => 'daily'
  add advertise_path, :changefreq => 'daily'
  add about_path, :changefreq => 'daily'
  add contactus_path, :changefreq => 'daily'
  add concerts_events_path, :changefreq => 'daily'
  add exhibition_events_path, :changefreq => 'daily'
  add networking_events_path, :changefreq => 'daily'
  add bazaar_festival_events_path, :changefreq => 'daily'
  add seminar_and_workshop_events_path, :changefreq => 'daily'
  add education_events_path, :changefreq => 'daily'
  add social_events_path, :changefreq => 'daily'
  add performance_events_path, :changefreq => 'daily'
  add competition_events_path, :changefreq => 'daily'
  add festive_events_path, :changefreq => 'daily'
  Event.all.each do |x|
    add  dtl_event_path(x.slug), :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Article.all.each do |x|
    add  dtl_article_path(x.category.parent.slug, x.category.slug, x.slug), :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Writer.all.each do |x|
    add writers_path(x.slug), :changefreq => 'monthly', :lastmod => x.articles.first.updated_at rescue '-'
  end
end
