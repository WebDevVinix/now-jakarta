Rails.application.routes.draw do
  resources :notifications
  resources :devices
  resources :directories
  resources :image_sliders
  resources :tags
  mount Ckeditor::Engine => '/ckeditor'

  root 'page#index'
  get 'dashboard' => "dashboard#index"

  get 'banners/remover' => 'banners#remover', as: :banners_remover
  get 'banners/article' => 'banners#article', as: :banners_article
  get 'redirect' => 'banners#counter', as: :banners_counter
  get 'send-report-banner' => 'banners#send_report_banner', as: :send_report_banner
  get 'banners/active' => 'banners#active', as: :banners_active
  get 'banners/trash' => 'banners#trash', as: :banners_trash
  resources :banners
  post 'banners/editbanner' => 'banners#editbanner', as: :banners_editbanner
  resources :galleries do
    collection { post :import }
    resources :images, :only => [:create, :destroy]
  end
  resources :videos
  resources :subscribes

  resources :sliders do
    put :sort, on: :collection
  end

  resources :users
  devise_for :users,
  path_names: {
    sign_in: 'digital/login',
    sign_out: 'digital/logout'
  }

  resources :writers do
    collection { post :import }
  end
  resources :contacts

  get 'tagged' => 'page#tagged', as: :tagged_article
  get '/city-updates', to: redirect('/updates', status: 301)
  get '/kids-family', to: redirect('/life/kids-family', status: 301)
  get '/lifestyle', to: redirect('/life', status: 301)
  get '/others', to: redirect('/people', status: 301)
  get '/our-contributors', to: redirect('/about', status: 301)
  get '/soapbox', to: redirect('/people/soapbox', status: 301)
  get '/magazine-issue/money-finance/brikcoin-a-eryptocurrency-for-the-value-of-the-absence-of-plastic', to: redirect('/magazine-issue/money-finance/brikcoin-a-cryptocurrency-for-the-value-of-the-absence-of-plastic', status: 301)
  get '/dining/foodies-corner/ultraviolet-by-paul-piret-multi-sensory-dining-at-its-best', to: redirect('/dining/foodies-corner/ultraviolet-by-paul-pairet-multi-sensory-dining-at-its-best', status: 301)
  get '/art-and-culture/arts/bartega-paint-sip-painting-under-influence', to: redirect('/art-and-culture/arts/bartega-studio-inspiring-creativity-through-social-painting', status: 301)
  get '/dining/culinary-talk/riana-bismaraks-favourite-restaurant', to: redirect('/dining/culinary-talk/5-top-jakarta-restaurants-in-riana-bismaraks-list', status: 301)
  get '/art-and-culture/arts/dedication-chairil-anwar', to: redirect('/art-and-culture/arts/bio-fantasy-melisa-sunjayas-dedication-to-chairil-anwar-bio-fantasy-melisa-sunjayas-dedication-to-chairil-now-jakarta', status: 301)
  get 'search' => 'page#search', as: :search
  get '/refined-dining-experience', to: redirect('/travel/archipelago-diaries/while-in-bali-go-to-these-top-restaurants-for-a-refined-dining-experience', status: 301)
  get '/travel/archipelago-diaries/refined-dining-experience', to: redirect('/travel/archipelago-diaries/while-in-bali-go-to-these-top-restaurants-for-a-refined-dining-experience', status: 301)
  get 'events/concerts'=> 'events#detail', as: :concerts_events
  get 'events/exhibition'=> 'events#detail', as: :exhibition_events
  get 'events/networking'=> 'events#detail', as: :networking_events
  get 'events/bazaar-and-festival'=> 'events#detail', as: :bazaar_festival_events
  get 'events/seminar-and-workshop'=> 'events#detail', as: :seminar_and_workshop_events
  get 'events/education'=> 'events#detail', as: :education_events
  get 'events/social'=> 'events#detail', as: :social_events
  get 'events/performance'=> 'events#detail', as: :performance_events
  get 'events/competition'=> 'events#detail', as: :competition_events
  get 'events/festive'=> 'events#detail', as: :festive_events
  get 'events/webinar'=> 'events#detail', as: :webinar_events

  get '/city-guides/kota-tua' => 'page#detail_area', as: :detail_area
  get 'latest-articles' => 'page#latest', as: :latest
  get '/list-event' => 'events#index', as: :list_event
  get '/list-article' => 'articles#index', as: :list_article
  get '/export-article' => 'articles#export', as: :export_article
  get '/list-subscriber' => 'subscribes#index', as: :list_subscriber
  get '/events' => 'page#events', as: :all_event

  get '/categories/:id/edit', to: 'categories#edit'
  get '/events/new', to: 'events#new'
  get '/events/:id', to: 'events#show', as: :dtl_event
  get '/events/:id/edit', to: 'events#edit'

  get '/city-guide' => 'categories#detail', as: :city_guide
  get '/photo-stories' => 'page#photos', as: :photos
  get 'dining' => 'categories#detail', as: :dining
  get 'people' => 'categories#detail', as: :people
  get 'art-and-culture' => 'categories#detail', as: :art_and_culture
  get 'life' => 'categories#detail', as: :life
  get 'updates' => 'categories#detail', as: :updates
  get 'events' => 'categories#detail', as: :nearest_event
  get 'business' => 'categories#detail', as: :business
  get 'travel' => 'categories#detail', as: :travel
  get 'home-life' => 'categories#detail', as: :home_life
  get 'special-offers' => 'categories#detail', as: :deals
  get 'magazine-issue' => 'categories#detail', as: :themes_articles
  get 'latest-articles' => 'categories#detail', as: :allpost
  get 'commitment' => 'categories#detail', as: :commitment
  get 'contest' => 'categories#detail', as: :contest
  get 'subscribe' => 'page#subscribe', as: :subscribe_now



  get 'advertise' => 'page#advertise', as: :advertise
  get 'about' => 'page#about', as: :about
  get 'contact-us' => 'page#contact', as: :contactus
  get 'njsc-2019-winner' => 'page#njsc_2019_winner', as: :njsc_winner
  get 'promotion' => 'page#promotion', as: :promotion
  get 'privacy-policy' => 'page#privacy', as: :privacy

  post "/add_device" => "devices#add_device", :as => :add_device

  get 'dashboard/index'
  get 'page/index'
  get 'page/about'
  get 'page/advertise'
  get 'page/contact'

  resources :categories do
    collection { post :import }
  end

  resources :events do
    collection { post :import }
  end

  get 'articles/trash/:id' => 'articles#trash', as: :articles_trash
  resources :articles, path: '' do
    collection { post :import }
  end

  get '/categories/:id', to: 'categories#show', as: :dtl_category
  get ':category_name/:sub/', to: 'categories#detail', as: :dtl_sub_category
  get ':category_name/:sub/:id', to: 'articles#show', as: :dtl_article
  get ':category_name/:sub/:id/edit', to: 'articles#edit'
end
