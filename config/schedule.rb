every 2.minute, mailto: 'aulya@phoenix.co.id' do
  rake "sitemap:refresh"
end

every 1.day, at: '7:00 pm' do
    rake "db:populate:send_automation_report_banner RAILS_ENV=production"
end
